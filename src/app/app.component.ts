import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Input } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  numberForm!: FormGroup;

  nombre: number = 0;
  nbrTable: number = 0;
  nbrLigne = [];
  

  ngOnInit(): void {
    this.numberForm = new FormGroup(
      {
        nombre: new FormControl(),
        nbrTable: new FormControl(),
      }
    )
  
  }
  
  submitedNumber(): void {
    if(this.numberForm.value.nombre === null){
      this.numberForm.value.nombre = 0;
    }
    if (this.numberForm.value.nbrTable === null ){
      this.numberForm.value.nbrTable = 10; 
    }
    
    this.nombre = this.numberForm.value.nombre;
    this.nbrTable = this.numberForm.value.nbrTable;
    
  }

}
