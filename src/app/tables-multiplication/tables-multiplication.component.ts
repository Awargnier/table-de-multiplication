import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tables-multiplication',
  templateUrl: './tables-multiplication.component.html',
  styleUrls: ['./tables-multiplication.component.scss']
})
export class TablesMultiplicationComponent implements OnInit {

  @Input() nbrTable!: number;
  

  nombreTotal: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  arrayTable: number[] = [];
 
  constructor() {
    
  }

  ngOnInit(): void {
    
  }

  nombreTable(){
    if (this.nbrTable < this.nombreTotal.length) {
      for(let i = 1;i<this.nbrTable+1;i++){
        this.arrayTable[i] = i;
      }
    }
    else if(this.nbrTable > this.nombreTotal.length){
      for(let j = 1;j<this.nbrTable+1;j++){
        this.arrayTable[j] = j;
      }
    }
    else if(this.nbrTable == this.nombreTotal.length){
      for(let k = 1;k<this.nbrTable+1;k++){
        this.arrayTable[k] = k;
      }
    }
    return this.arrayTable ;
  }
  
}
